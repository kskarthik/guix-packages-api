# A simple GNU Guix Package Search REST API using Deno

## Requirements

- Deno

## Installation

`deno run --allow-net main.js`

The api is available on `http://localhost:7777`

## Environment Variables

- `GUIX_SYNC_INTERVAL` : Set sync interval in minutes

## API Endpoints

### `/pkg=<packagename>`

Returns Object with package details

Example: `/pkg=emacs`

```json
{
  "guix_status": 0,
  "guix_result": {
    "name": "emacs",
    "version": "27.2",
    "source": [
      {
        "type": "url",
        "urls": [
          "https://ftpmirror.gnu.org/gnu/emacs/emacs-27.2.tar.xz",
          "ftp://ftp.cs.tu-berlin.de/pub/gnu/emacs/emacs-27.2.tar.xz",
          "ftp://ftp.funet.fi/pub/mirrors/ftp.gnu.org/gnu/emacs/emacs-27.2.tar.xz",
          "http://ftp.gnu.org/pub/gnu/emacs/emacs-27.2.tar.xz"
        ],
        "integrity": "sha256-tKfMTnjmPzeGJOCRkhW5EK9bsqCvyBn60pgnLp9Awbk="
      }
    ],
    "synopsis": "The extensible, customizable, self-documenting text editor",
    "homepage": "https://www.gnu.org/software/emacs/",
    "location": "gnu/packages/emacs.scm:81"
  }
}
```

### `/search=<search term>`

Returns an array of package objects matching the search term

Example: `/search=email client`

```json
{
  "guix_status": 0,
  "guix_result": [
    {
      "name": "icedove",
      "version": "91.4.0",
      "cpe_name": "thunderbird_esr",
      "source": [
        {
          "type": "hg",
          "hg_url": "https://hg.mozilla.org/l10n/compare-locales/",
          "hg_changeset": "RELEASE_8_1_0"
        },
        {
          "type": "url",
          "urls": [
            "https://ftp.mozilla.org/pub/firefox/releases/91.4.0esr/source/firefox-91.4.0esr.source.tar.xz"
          ],
          "integrity": "sha256-5yKCnbSQ+TMnEqgcNZllQXN7v7BSMtRxkP7necT8syc="
        },
        {
          "type": "git",
          "git_url": "git://git.savannah.gnu.org/gnuzilla.git",
          "git_ref": "dd79d69e5dc6e6e751195001f322b30746be6903"
        }
      ],
      "synopsis": "Rebranded Mozilla Thunderbird email client",
      "homepage": "https://www.thunderbird.net",
      "location": "gnu/packages/gnuzilla.scm:1333"
    },
    {
      "name": "icedove-wayland",
      "version": "91.4.0",
      "cpe_name": "thunderbird_esr",
      "source": [
        {
          "type": "hg",
          "hg_url": "https://hg.mozilla.org/l10n/compare-locales/",
          "hg_changeset": "RELEASE_8_1_0"
        },
        {
          "type": "url",
          "urls": [
            "https://ftp.mozilla.org/pub/firefox/releases/91.4.0esr/source/firefox-91.4.0esr.source.tar.xz"
          ],
          "integrity": "sha256-5yKCnbSQ+TMnEqgcNZllQXN7v7BSMtRxkP7necT8syc="
        },
        {
          "type": "git",
          "git_url": "git://git.savannah.gnu.org/gnuzilla.git",
          "git_ref": "dd79d69e5dc6e6e751195001f322b30746be6903"
        }
      ],
      "synopsis": "Rebranded Mozilla Thunderbird email client",
      "homepage": "https://www.thunderbird.net",
      "location": "gnu/packages/gnuzilla.scm:1648"
    },
    {
      "name": "kmail",
      "version": "20.04.1",
      "source": [
        {
          "type": "url",
          "urls": [
            "http://download.kde.org/stable/release-service/20.04.1/src/kmail-20.04.1.tar.xz",
            "http://download.kde.org/Attic/stable/release-service/20.04.1/src/kmail-20.04.1.tar.xz",
            "http://mirror.easyname.at/kde/stable/release-service/20.04.1/src/kmail-20.04.1.tar.xz",
            "http://mirror.karneval.cz/pub/kde/stable/release-service/20.04.1/src/kmail-20.04.1.tar.xz",
            "http://ftp.fi.muni.cz/pub/kde/stable/release-service/20.04.1/src/kmail-20.04.1.tar.xz",
            "http://mirror.oss.maxcdn.com/kde/stable/release-service/20.04.1/src/kmail-20.04.1.tar.xz",
            "http://ftp5.gwdg.de/pub/linux/kde/stable/release-service/20.04.1/src/kmail-20.04.1.tar.xz",
            "http://ftp-stud.fht-esslingen.de/Mirrors/ftp.kde.org/pub/kde/stable/release-service/20.04.1/src/kmail-20.04.1.tar.xz",
            "http://mirror.klaus-uwe.me/kde/ftp/stable/release-service/20.04.1/src/kmail-20.04.1.tar.xz",
            "http://kde.beta.mirror.ga/stable/release-service/20.04.1/src/kmail-20.04.1.tar.xz",
            "http://kde.alpha.mirror.ga/stable/release-service/20.04.1/src/kmail-20.04.1.tar.xz",
            "http://mirror.netcologne.de/kde/stable/release-service/20.04.1/src/kmail-20.04.1.tar.xz",
            "http://vesta.informatik.rwth-aachen.de/ftp/pub/mirror/kde/stable/release-service/20.04.1/src/kmail-20.04.1.tar.xz",
            "http://ftp.rz.uni-wuerzburg.de/pub/unix/kde/stable/release-service/20.04.1/src/kmail-20.04.1.tar.xz",
            "http://mirrors.dotsrc.org/kde/stable/release-service/20.04.1/src/kmail-20.04.1.tar.xz",
            "http://ftp.funet.fi/pub/mirrors/ftp.kde.org/pub/kde/stable/release-service/20.04.1/src/kmail-20.04.1.tar.xz",
            "http://kde-mirror.freenux.org/stable/release-service/20.04.1/src/kmail-20.04.1.tar.xz",
            "http://mirrors.ircam.fr/pub/KDE/stable/release-service/20.04.1/src/kmail-20.04.1.tar.xz",
            "http://www-ftp.lip6.fr/pub/X11/kde/stable/release-service/20.04.1/src/kmail-20.04.1.tar.xz",
            "http://fr2.rpmfind.net/linux/KDE/stable/release-service/20.04.1/src/kmail-20.04.1.tar.xz",
            "http://kde.mirror.anlx.net/stable/release-service/20.04.1/src/kmail-20.04.1.tar.xz",
            "http://www.mirrorservice.org/sites/ftp.kde.org/pub/kde/stable/release-service/20.04.1/src/kmail-20.04.1.tar.xz",
            "http://ftp.heanet.ie/mirrors/ftp.kde.org/stable/release-service/20.04.1/src/kmail-20.04.1.tar.xz",
            "http://ftp.nluug.nl/pub/windowing/kde/stable/release-service/20.04.1/src/kmail-20.04.1.tar.xz",
            "http://ftp.surfnet.nl/windowing/kde/stable/release-service/20.04.1/src/kmail-20.04.1.tar.xz",
            "http://ftp.icm.edu.pl/pub/unix/kde/stable/release-service/20.04.1/src/kmail-20.04.1.tar.xz",
            "http://ftp.pbone.net/pub/kde/stable/release-service/20.04.1/src/kmail-20.04.1.tar.xz",
            "http://piotrkosoft.net/pub/mirrors/ftp.kde.org/stable/release-service/20.04.1/src/kmail-20.04.1.tar.xz",
            "http://mirrors.fe.up.pt/pub/kde/stable/release-service/20.04.1/src/kmail-20.04.1.tar.xz",
            "http://ftp.iasi.roedu.net/pub/mirrors/ftp.kde.org/stable/release-service/20.04.1/src/kmail-20.04.1.tar.xz",
            "http://ftp.acc.umu.se/mirror/kde.org/ftp/stable/release-service/20.04.1/src/kmail-20.04.1.tar.xz",
            "http://kde.ip-connect.vn.ua/stable/release-service/20.04.1/src/kmail-20.04.1.tar.xz",
            "http://mirror.its.dal.ca/kde/stable/release-service/20.04.1/src/kmail-20.04.1.tar.xz",
            "http://mirror.csclub.uwaterloo.ca/kde/stable/release-service/20.04.1/src/kmail-20.04.1.tar.xz",
            "http://mirror.cc.columbia.edu/pub/software/kde/stable/release-service/20.04.1/src/kmail-20.04.1.tar.xz",
            "http://kde.mirrors.hoobly.com/stable/release-service/20.04.1/src/kmail-20.04.1.tar.xz",
            "http://ftp.ussg.iu.edu/kde/stable/release-service/20.04.1/src/kmail-20.04.1.tar.xz",
            "http://mirrors.mit.edu/kde/stable/release-service/20.04.1/src/kmail-20.04.1.tar.xz",
            "http://kde.mirrors.tds.net/pub/kde/stable/release-service/20.04.1/src/kmail-20.04.1.tar.xz",
            "http://ftp.kddlabs.co.jp/pub/X11/kde/stable/release-service/20.04.1/src/kmail-20.04.1.tar.xz",
            "http://kde.mirror.uber.com.au/stable/release-service/20.04.1/src/kmail-20.04.1.tar.xz"
          ],
          "integrity": "sha256-d/y9ba2dbc8wLbhZG9rv6DeTFJOakMnKNVWrW+LvDhs="
        }
      ],
      "synopsis": "Full featured graphical email client",
      "homepage": "https://kontact.kde.org/components/kmail.html",
      "location": "gnu/packages/kde-pim.scm:1006"
    },
    {
      "name": "rust-lettre",
      "version": "0.9.6",
      "source": [
        {
          "type": "url",
          "urls": ["https://crates.io/api/v1/crates/lettre/0.9.6/download"],
          "integrity": "sha256-hu2GdxOJdbVzq0lJw1YTkxpK3erdCopqoDJ+KpeWYN4="
        }
      ],
      "synopsis": "Rust email client",
      "homepage": "https://lettre.rs",
      "location": "gnu/packages/crates-io.scm:26332"
    },
    {
      "name": "sylpheed",
      "version": "3.7.0",
      "source": [
        {
          "type": "url",
          "urls": [
            "https://sylpheed.sraoss.jp/sylpheed/v3.7/sylpheed-3.7.0.tar.xz"
          ],
          "integrity": "sha256-DWvF60b10KRjdXxivICwDSGbKUs8NmKEDkVA9tsuPkk="
        }
      ],
      "synopsis": "Lightweight GTK+ email client",
      "homepage": "https://sylpheed.sraoss.jp/en/",
      "location": "gnu/packages/mail.scm:4119"
    }
  ]
}
```

## License

This software is licensed under ~AGPLv3~. You can obtain a copy of the license from [https://www.gnu.org/licenses/agpl-3.0.txt](https://www.gnu.org/licenses/agpl-3.0.txt)
