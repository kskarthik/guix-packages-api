/**
 * Title: Guix Package Info API
 * Author: Sai Karthik <kskarthik@disroot.org>
 * License: AGPLv3 (https://www.gnu.org/licenses/agpl-3.0.txt)
 **/
import { Application } from "https://deno.land/x/abc@v1.3.3/mod.ts";

// Initialize the web server
const app = new Application();

/**
 * Start the webserver at port 7777
 * @param {} name
 * @param {} c
 * @returns {}
 */
app
  .get("/search=:name", packageSearch)
  .get("/pkg=:name", packageInfo)
  .start({ port: 7777 });

// Status codes
const statusCode = {
  success: 0,
  failure: 1,
};
/**
 * returns all matching packages for supplied search term
 * @param {} c
 * @returns {}
 */
function packageSearch(c) {
  let { name } = c.params;

  // santize search query
  if (new RegExp("%20").test(name)) {
    name = name.replace("%20", " ");
  }

  let result = { guix_status: statusCode.failure, guix_result: [] };

  let term = new RegExp(name);
  for (let pkg in guixPackages) {
    // first check if the term matches the package name
    if (term.test(guixPackages[pkg].name)) {
      result.guix_status = statusCode.success;
      result.guix_result.push(guixPackages[pkg]);
    }
    // check if the search term matches the package description
    if (term.test(guixPackages[pkg].synopsis)) {
      result.guix_result.push(guixPackages[pkg]);
      result.guix_status = statusCode.success;
    }
  }
  return result;
}
/**
 * Return package info object when supplied with valid name
 * else return error status code
 * @param {} c
 * @returns {}
 */
function packageInfo(c) {
  const { name } = c.params;
  let response = { guix_status: statusCode.failure };
  for (let pkg in guixPackages) {
    if (guixPackages[pkg].name === name) {
      response.guix_status = statusCode.success;
      response["guix_result"] = guixPackages[pkg];
    }
  }

  return response;
}

let guixPackages = null;

/**
 * fetch packages.json from guix website
 * @returns {}
 */
function fetchPackages() {
  console.log(`Syncing guile package database 📡 @ ${new Date()}`);

  fetch("https://guix.gnu.org/packages.json", {
    mode: "cors",
    cache: "reload",
  })
    .then((r) => r.json())
    .then((json) => {
      console.log(`Package database is synced 🎉 @ ${new Date()}`);
      guixPackages = json;
    })
    .catch((e) => {
      guixPackages = null;
      console.log(e);
    });
}

/**
 * Refresh package database every 15 minutes
 * or as defined in GUIX_SYNC_INTERVAL environment variable
 */

const syncInterval = Deno.env.get("GUIX_SYNC_INTERVAL") || 15;

setInterval(fetchPackages, 1000 * (parseInt(syncInterval) * 60));

const message = `
Package Sync Interval: ${syncInterval} minute(s)
Server is running on http://localhost:7777 🚀
Press Ctrl+C to stop the server
`;

// Fetch package database on startup
if (guixPackages === null) {
  console.log(message);
  fetchPackages();
} else {
  console.log(message);
}
